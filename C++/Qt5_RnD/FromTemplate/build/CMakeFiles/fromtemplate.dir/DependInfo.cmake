# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/media/s4lprod/UM_Data/raspberry-pi-4-research/C++/Qt5_RnD/FromTemplate/build/fromtemplate_autogen/mocs_compilation.cpp" "/media/s4lprod/UM_Data/raspberry-pi-4-research/C++/Qt5_RnD/FromTemplate/build/CMakeFiles/fromtemplate.dir/fromtemplate_autogen/mocs_compilation.cpp.o"
  "/media/s4lprod/UM_Data/raspberry-pi-4-research/C++/Qt5_RnD/FromTemplate/src/fromtemplate.cpp" "/media/s4lprod/UM_Data/raspberry-pi-4-research/C++/Qt5_RnD/FromTemplate/build/CMakeFiles/fromtemplate.dir/src/fromtemplate.cpp.o"
  "/media/s4lprod/UM_Data/raspberry-pi-4-research/C++/Qt5_RnD/FromTemplate/src/main.cpp" "/media/s4lprod/UM_Data/raspberry-pi-4-research/C++/Qt5_RnD/FromTemplate/build/CMakeFiles/fromtemplate.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "fromtemplate_autogen/include"
  "/usr/include/aarch64-linux-gnu/qt5"
  "/usr/include/aarch64-linux-gnu/qt5/QtWidgets"
  "/usr/include/aarch64-linux-gnu/qt5/QtGui"
  "/usr/include/aarch64-linux-gnu/qt5/QtCore"
  "/usr/lib/aarch64-linux-gnu/qt5/mkspecs/linux-g++"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
