#include "fromtemplate.h"
#include "ui_fromtemplate.h"

FromTemplate::FromTemplate(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::FromTemplate)
{
    m_ui->setupUi(this);
}

FromTemplate::~FromTemplate() = default;
