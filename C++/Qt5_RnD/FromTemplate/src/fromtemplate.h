#ifndef FROMTEMPLATE_H
#define FROMTEMPLATE_H

#include <QMainWindow>
#include <QScopedPointer>

namespace Ui {
class FromTemplate;
}

class FromTemplate : public QMainWindow
{
    Q_OBJECT

public:
    explicit FromTemplate(QWidget *parent = nullptr);
    ~FromTemplate() override;

private:
    QScopedPointer<Ui::FromTemplate> m_ui;
};

#endif // FROMTEMPLATE_H
