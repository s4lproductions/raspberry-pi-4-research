#include <ranges>
#include <iostream>
#include <vector>

int main() {
	std::vector<int> elements {0, 1, 2, 3, 4, 5, 6};

	for (int current : elements | std::views::filter([] (int e) { return e % 2 == 0; })) {
		std::cout << current << " ";
	}
}