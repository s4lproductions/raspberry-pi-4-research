from gpiozero import Button, LED
from signal import pause


btn = Button(27)
led = LED(17)

btn.when_pressed = led.on
btn.when_released = led.off

pause()
